package br.com.bd2.app;

import br.com.bd2.model.TipoLivroEnum;

public class EmprestimoDTO {
	private TipoLivroEnum situ;
	private long qntd;
	
		
	
	
	public EmprestimoDTO(TipoLivroEnum situ, long qntd) {
		super();
		this.situ = situ;
		this.qntd = qntd;
	}
	public TipoLivroEnum getSitu() {
		return situ;
	}
	public long getQntd() {
		return qntd;
	}
	public void setSitu(TipoLivroEnum situ) {
		this.situ = situ;
	}
	public void setQntd(long qntd) {
		this.qntd = qntd;
	}
	@Override
	public String toString() {
		return "EmprestimoDTO [situ=" + situ + ", qntd=" + qntd + "]";
	}
	
	
	
}
