package br.com.bd2.app;

import br.com.bd2.model.TipoLivroEnum;

public class App {

	public static void main(String[] args) throws InterruptedException {
		CargarDados.cargar();
		
	Thread.sleep(6000);
		
		ConsultaDados.consultarPorTipo(TipoLivroEnum.E);
		ConsultaDados.consultarPorTipo(TipoLivroEnum.C);
		
		Thread.sleep(3000);
		
	ConsultaDados.consultarQtdEmprestimoPorLivro();
	}

}
