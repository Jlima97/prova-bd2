package br.com.bd2.app;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import br.com.bd2.model.LivroModel;
import br.com.bd2.model.TipoLivroEnum;

public class ConsultaDados {

	
	public static void consultarPorTipo(TipoLivroEnum tipoLivro) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Prova-bd2");
		EntityManager em = emf.createEntityManager();
		
		
		
		String oql1 = "Select l from LivroModel l where l.tipoLivro = :tipoLivro ";
		TypedQuery<LivroModel> query = em.createQuery(oql1, LivroModel.class);
		query.setParameter("tipoLivro", tipoLivro);
		
		List<LivroModel> livros = query.getResultList();
		for (LivroModel livro : livros) {
		    System.out.println(livro);
		}


		em.close();
		emf.close();
		
	}
	public static void consultarQtdEmprestimoPorLivro() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Prova-bd2");
		EntityManager em = emf.createEntityManager();
		String oql3 = "select new " + EmprestimoDTO.class.getCanonicalName()
				+ "(l.tipoLivro, count(*)) from LivroModel l group by l.tipoLivro";
		TypedQuery<EmprestimoDTO> query3 = em.createQuery(oql3, EmprestimoDTO.class);
		List<EmprestimoDTO> result3 = query3.getResultList();
		System.out.println("Result3:");
		for (EmprestimoDTO resultadoDTO : result3) {
			System.out.println(resultadoDTO);
		}

		
		
	}
	
}
