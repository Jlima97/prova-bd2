package br.com.bd2.app;

import java.time.LocalDate;
import java.util.Arrays;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.bd2.model.EditoraModel;
import br.com.bd2.model.EmprestimoModel;
import br.com.bd2.model.LeitorModel;
import br.com.bd2.model.LivroModel;
import br.com.bd2.model.TipoLivroEnum;

public class CargarDados {

	public static void cargar() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Prova-bd2");
		EntityManager em = emf.createEntityManager();

		EditoraModel editora = new EditoraModel();
		editora.setNome("alo");
		LivroModel lm = new LivroModel();
		
		lm.setIsbn("ABCDES");
		lm.setTipoLivro(TipoLivroEnum.C);
		lm.setTitulo("Dora aventureira");
		
		LivroModel lm2 = new LivroModel();
		
		lm2.setIsbn("Teste332");
		lm2.setTipoLivro(TipoLivroEnum.E);
		lm2.setTitulo("Guardiola");
		
		LivroModel lm3 = new LivroModel();
		
		lm3.setIsbn("Teste4343");
		lm3.setTipoLivro(TipoLivroEnum.E);
		lm3.setTitulo("Messi");
		
		editora.setLivros(Arrays.asList(lm,lm2, lm3));		

		
		
		LeitorModel letm = new LeitorModel();
		letm.setCpf("321321321");
		letm.setNome("Jean");
		letm.setTelefone(Arrays.asList("75884848"));
		
		EmprestimoModel emp1 = new EmprestimoModel();
        LocalDate hoje = LocalDate.now();		
        LocalDate dt2 = LocalDate.of(2020, 06, 20);		

		emp1.setLivro(lm);
		emp1.setLeitor(letm);
		emp1.setDataEmprestimo(hoje);
		emp1.setDataDevolucao(dt2);
		
		EmprestimoModel emp2 = new EmprestimoModel();
		emp2.setLivro(lm2);
		emp2.setLeitor(letm);
		emp2.setDataEmprestimo(hoje);
		emp2.setDataDevolucao(dt2);
		
		
		em.getTransaction().begin();
		em.persist(editora);
		em.persist(lm);
		em.persist(lm2);
		em.persist(lm3);
		em.persist(letm);
		em.persist(emp1);
		em.persist(emp2);
		em.getTransaction().commit();
		em.close();
		emf.close();
	}

	


}
