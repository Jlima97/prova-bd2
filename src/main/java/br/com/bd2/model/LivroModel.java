package br.com.bd2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class LivroModel {
	@Id
	@Column(length = 18)
	private String isbn;
	@Column(length = 200)
	private String titulo;
	
	@Column(columnDefinition="CHAR(1)")
	@Enumerated(EnumType.STRING)
	private TipoLivroEnum tipoLivro;
	
	
	


	public String getIsbn() {
		return isbn;
	}


	public String getTitulo() {
		return titulo;
	}


	public TipoLivroEnum getTipoLivro() {
		return tipoLivro;
	}


	


	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public void setTipoLivro(TipoLivroEnum tipoLivro) {
		this.tipoLivro = tipoLivro;
	}





	@Override
	public String toString() {
		return "LivroModel [isbn=" + isbn + ", titulo=" + titulo + ", tipoLivro=" + tipoLivro 
				+ "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LivroModel other = (LivroModel) obj;
		if (isbn == null) {
			if (other.isbn != null)
				return false;
		} else if (!isbn.equals(other.isbn))
			return false;
		return true;
	}

public LivroModel() {}
	
	
}



