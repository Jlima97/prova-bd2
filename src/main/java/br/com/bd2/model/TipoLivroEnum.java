package br.com.bd2.model;

public enum TipoLivroEnum {
	C("Consulta"), E("Emprestimo");

	private String codigo;
	
	private TipoLivroEnum(String codigo) {
		this.codigo = codigo;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public static TipoLivroEnum  valueOfCodigo(String codigo) {
		for (TipoLivroEnum  situacao : values()) {
			if (situacao.codigo.equalsIgnoreCase(codigo)) {
				return situacao;
			}
		}
		throw new IllegalArgumentException("Codigo " + codigo + " não encontrado em TipoLivroEnum.");
	}

}
