package br.com.bd2.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(name="sq_editora", sequenceName="seq_editora")

public class EditoraModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_editora" )

private Integer id;
@Column(length = 200, nullable = false)
 private String nome;


@OneToMany(cascade = CascadeType.ALL,
        orphanRemoval = true)
//- professor, fiquei em duvida se deveria usar esse - @JoinColumn(name="editora_id")
private List<LivroModel> livros;


public Integer getId() {
	return id;
}


public String getNome() {
	return nome;
}


public List<LivroModel> getLivros() {
	return livros;
}


public void setId(Integer id) {
	this.id = id;
}


public void setNome(String nome) {
	this.nome = nome;
}


public void setLivros(List<LivroModel> livros) {
	this.livros = livros;
}


@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
}


@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	EditoraModel other = (EditoraModel) obj;
	if (id == null) {
		if (other.id != null)
			return false;
	} else if (!id.equals(other.id))
		return false;
	return true;
}


public EditoraModel() {}


	
	
}
