package br.com.bd2.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity

@SequenceGenerator(name="sq_emprestimo", sequenceName="seq_emprestimo")
@Table(uniqueConstraints = 
@UniqueConstraint(columnNames = {"livro_id", "data_emprestimo"}, name = "empresa_uk"))
public class EmprestimoModel {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sq_emprestimo")
	private Long id;
	
	@OneToOne
    @JoinColumn(name="livro_id")
	private LivroModel livro;
	
	@OneToOne
    @JoinColumn(name="leitor_ID")
	private LeitorModel leitor;
	
    @Column(name = "data_emprestimo")

	private LocalDate dataEmprestimo;

	private LocalDate dataDevolucao;
	public Long getId() {
		return id;
	}
	public LivroModel getLivro() {
		return livro;
	}
	public LeitorModel getLeitor() {
		return leitor;
	}
	public LocalDate getDataEmprestimo() {
		return dataEmprestimo;
	}
	public LocalDate getDataDevolucao() {
		return dataDevolucao;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setLivro(LivroModel livro) {
		this.livro = livro;
	}
	public void setLeitor(LeitorModel leitor) {
		this.leitor = leitor;
	}
	public void setDataEmprestimo(LocalDate dataEmprestimo) {
		this.dataEmprestimo = dataEmprestimo;
	}
	public void setDataDevolucao(LocalDate dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

    
    public EmprestimoModel() {   }
	@Override
	public String toString() {
		return "EmprestimoModel [id=" + id + ", livro=" + livro + ", leitor=" + leitor + ", dataEmprestimo="
				+ dataEmprestimo + ", dataDevolucao=" + dataDevolucao + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmprestimoModel other = (EmprestimoModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
    
    
    
    
}
