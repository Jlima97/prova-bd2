package br.com.bd2.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LeitorModel {

@Id
@Column(columnDefinition="CHAR(11)")
private String cpf;

@Column(length = 50)
private String nome;


@ElementCollection
@Column(length = 12)
private List<String> telefone;

public LeitorModel() {}

public String getCpf() {
	return cpf;
}

public String getNome() {
	return nome;
}

public List<String> getTelefone() {
	return telefone;
}

public void setCpf(String cpf) {
	this.cpf = cpf;
}

public void setNome(String nome) {
	this.nome = nome;
}

public void setTelefone(List<String> telefone) {
	this.telefone = telefone;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	LeitorModel other = (LeitorModel) obj;
	if (cpf == null) {
		if (other.cpf != null)
			return false;
	} else if (!cpf.equals(other.cpf))
		return false;
	return true;
}

@Override
public String toString() {
	return "LeitorModel [cpf=" + cpf + ", nome=" + nome + ", telefone=" + telefone + "]";
}



	
}
